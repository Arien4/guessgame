package com.epam.stage2;

import java.util.List;

public class View {
    public void printGreeting(int min, int max) {
        System.out.printf("Please enter the number from the range (%d, %d)", min, max);
        System.out.println();
    }

    public void printRetryRangeMessage(int min, int max) {
        System.out.printf("The number is out of range (%d, %d)", min, max);
        System.out.println();
    }

    public void printRetryMessage() {
        System.out.println("Your number is not valid");
    }

    public void printGreaterMessage() {
        System.out.println("Your number is greater than expected");
    }

    public void printLessMessage() {
        System.out.println("Your number is less than expected");
    }

    public void printBingoMessage(List<Integer> attempts) {
        System.out.println("Bingo!");
        System.out.println("Your attempts " + attempts);
    }
}
